Crear un blog con los siguientes requisitos técnicos

HTML
•	La estructura de las páginas creadas dinámicamente tendrá que tener sentido a nivel semántico
•	Utilizaremos un formulario correctamente validado para añadir datos

CSS
•	Usaremos Flex para posicionar los elementos (alternativamente podremos usar otro esquema de posicionamiento como Grid o la Grid de Bootstrap)
•	EXTRA: se buscará un sentido y orden a las clases definidas en el CSS (usando por ejemplo el sistema de nombrado BEM u otro similar)
•	EXTRA: la web será responsive, contemplando el tamaño móvil, tablet y desktop
•	EXTRA: desarrollaremos nuestro CSS con la ayuda de SASS.

JavaScript

•	Tenemos que implementar nuestra web como una SPA (single page application), lo que quiere decir que...
o	Los enlaces no deberán cargar un nuevo documento, dispararán la modificación del documento actual por medio de JS
o	Los formularios no deberán enviarse, los datos se recogerán desde JS y, si fuese necesario, se modificará el documento actual por medio de JS
•	Los datos se obtendrán de una API REST por medio de la API Fetch
•	Aparte de los elementos <script> necesarios para cargar nuestra aplicación, en el <body> de nuestro fichero HTML solo tendremos un elemento <div> en el que inyectaremos los demás elementos que iremos creando dinámicamente mediante JS usando...
o	Métodos del objeto document como createElement y otros de propios de los nodos como appendChild
o	Manejadores de evento registrados con addEventListener
•	La web tendrá elementos comunes a todas sus páginas, por ejemplo, una barra de navegación y/o un footer con información
•	Se espera la web tenga al menos tres vistas...
o	Una home con un menú general (por ejemplo, mostrando distintas categorías)
o	Un listado resultado de pulsar en una de las opciones de la home (por ejemplo, al pulsar en una categoría se muestran en forma de lista todos los elementos que coinciden con esa clasificación)
o	El detalle de un elemento (por ejemplo, al pinchar en un elemento de la anterior lista nos llevaría a una página con todos los detalles de este)
•	EXTRA: utilizaremos funciones para no repetir una y otra vez las mismas sentencias, funciones que tendrán resposabilidades lo más concretas posible
•	EXTRA: utilizaremos ficheros distintos que cargaremos desde el HTML con la etiqueta <script>, agrupando las funciones por resposabilidad
•	EXTRA: los listados se cargarán con lazy loading, o sea, la primera vez cargarán algunos resultados y hasta que no hagamos scroll no cargarán los siguientes
•	EXTRA: en la barra de navegación habrá un buscador que nos permite obtener elementos que coincidan con la cadena de búsqueda introducida
•	EXTRA: el buscador sugerirá al empezar a escribir algunas cadenas de búsqueda en las que el usuario podría estar interesado: al pulsar en una sugerencia se lanzará la búsqueda en cuestión
•	EXTRA: la web almacenará localmente ciertos datos añadidos por el usuario (podría ser que por ejemplo hubiese una página de configuración en la elegir ciertas preferencias, o que se almacenen elementos de los listados en las búsquedas en una lista de favoritos, o que se puedan añadir notas ligadas a los elementos, o comentarios de los lectores, o cualquier otra idea que trabaje con la persistencia local del navegador)
•	EXTRA: en los listados podría haber un filtro que modificase la vista según los elementos coincidan o no con el texto de filtrado
