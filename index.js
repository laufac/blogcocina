/**Variables globales*/
const blog$$ = document.querySelector('.blog');
let usersArray = [];
let commentsArray = [];
let PostId;
let postComp;
/*for (let i = 1; i<17; i++){
  localStorage.setItem('PostSum' + i, 0);
}*/

//let UserNow = null;

/* Excepto en posición 0 que va a ir guardado si logueado o no logueado y en posición 1 el id del usuario logueado: */
//sessionStorage[0]= 'true' / 'false'
//sessionStorage[userId]= '2'
//sessionStorage.setItem(idUsuario_timeStamp,comentario); //ó sessionStorage[idUsuario]=comentario


window.onload = function () { //Una vez que la página está cargada, llama a las funciones
  ObtenerUsuarios();
  header();
  mostrarComponentes();
  //limpiarLocalStorage();
}

function reset(){
  location.reload();
}

function insertAfter(newNode, referenceNode) {
  referenceNode.parentNode.insertBefore(newNode, referenceNode.nextSibling);
}

/*************************************** HEADER *****************************************************************/

function FormatoHeader(){
  const header$$ = document.createElement('header');

  const logo$$ = document.createElement('img');
  logo$$.classList.add('header__logo');
  logo$$.src = '/src/assets/img/logo4.png';

  logo$$.addEventListener('click', function(e) {
    reset();
  });

  const header_menu$$ = document.createElement('div');
  header_menu$$.classList.add('header_menu');

  const hamburger$$ = document.createElement('div');
  hamburger$$.classList.add('hamburger');

  const top$$ = document.createElement('div');
  top$$.classList.add('_layer', '-top');
  const mid$$ = document.createElement('div');
  mid$$.classList.add('_layer', '-mid');
  const bottom$$ = document.createElement('div');
  bottom$$.classList.add('_layer', '-bottom');

  hamburger$$.appendChild(top$$);
  hamburger$$.appendChild(mid$$);
  hamburger$$.appendChild(bottom$$);

  
  const nav$$ = document.createElement('nav');
  nav$$.classList.add('menuppal');

  const ul$$ = document.createElement('ul');
  ul$$.classList.add('menuppal__ul');
  const li1$$ = document.createElement('li');
  const a1$$ = document.createElement('a');
  a1$$.setAttribute('href', "#");
  a1$$.textContent = "Buscar";
  a1$$.classList.add('menuppal__Buscar');
  li1$$.appendChild(a1$$);
  const li2$$ = document.createElement('li');
  const a2$$ = document.createElement('a');
  a2$$.setAttribute('href', "#");

  if(localStorage.getItem('log') === 'true'){
    a2$$.textContent = "Logout";
    a2$$.classList.add('menuppal__Logout');
  }else{
    a2$$.textContent = "Login";
    a2$$.classList.add('menuppal__Login');
  }
  
  li2$$.appendChild(a2$$);

  ul$$.appendChild(li1$$);
  ul$$.appendChild(li2$$);
  nav$$.appendChild(ul$$);

  header_menu$$.appendChild(hamburger$$);
  header_menu$$.appendChild(nav$$);

  header$$.appendChild(logo$$);
  header$$.appendChild(header_menu$$);

  blog$$.appendChild(header$$);

}

function header(){
  FormatoHeader();

  // selector
var menu = document.querySelector('.hamburger');

// method
function toggleMenu (event) {
  this.classList.toggle('is-active');
  const nav$$ = document.querySelector( ".menuppal" );
  document.querySelector( ".menuppal" ).classList.toggle("is_active");
  event.preventDefault();
  const login$$ = document.querySelector( ".menuppal__Login" );
  const logout$$ = document.querySelector( ".menuppal__Logout" );

  const buscar$$ = document.querySelector( ".menuppal__Login" );
  const ul$$ = document.querySelector( ".menuppal__ul" );
  
  if(login$$ != null){
    login$$.addEventListener('click', function(e) {
      if (ul$$){
        nav$$.removeChild(ul$$);
      }
      
      login(nav$$);
    });
  }
  if(logout$$ != null){
    logout$$.addEventListener('click', function(e) {
      /*limpiarLocalStorage();*/
      localStorage.removeItem('log');
      reset();
      alert('LOG OUT');
    });
  }
}

// event
menu.addEventListener('click', toggleMenu, false);
}

/**************************************** PAGINA PRINCIPAL ****************************************************/

//Función que recibe el objeto post y le da forma en un componente
function componentePost(post){
  const componente$$ = document.createElement('div');
  componente$$.classList.add('ppal__componente');

  const img$$ = document.createElement('img');
  img$$.classList.add('ppal__componente__img');
  img$$.src = post.img;
  componente$$.appendChild(img$$);

  const div$$ = document.createElement('div');
  div$$.classList.add('ppal__componente__txt');
  const h2$$ = document.createElement('h2');
  h2$$.textContent = post.title;
  const p$$ = document.createElement('p');
  p$$.textContent = post.body.slice(0, 200) + "...";   ///en principio todo pero tengo que hacer un for que me coja solo el principio

  div$$.appendChild(h2$$);
  div$$.appendChild(p$$);

  componente$$.appendChild(div$$);

  return componente$$;
}

//Función que hace el fetch de cada uno de los objetos 'post' del array post y llama a la función 
//commponente para que los dibuje uno debajo del otro
function mostrarComponentes(){
  const main$$ = document.createElement('main');
  fetch('http://localhost:3000/posts').then(res => res.json()).then(function(posts){
    for (const post of posts) {
      const componente$$ = componentePost(post);
      main$$.appendChild(componente$$);
      
      componente$$.addEventListener('click', function(e) {
        ObtenerComentarios(main$$, post);
      });
    }
    });
    blog$$.appendChild(main$$);
}

/****************************************POST RECETA****************************************************/

function postReceta(main$$, post){
  PostId = post.id;
  postComp = post;
  let ComSum = parseInt(localStorage.getItem('PostSum' + PostId));
  console.log('suma coments ' +ComSum);

  blog$$.removeChild(main$$);
  main$$ =  document.createElement('main');
  main$$.classList.add('main__receta');
  
  const h1$$ = document.createElement('h1');
  h1$$.textContent = post.title;
  const hr$$ = document.createElement('hr')
  const fecha$$ = document.createElement('h3');
  fecha$$.textContent = post.date;
  const img$$ = document.createElement('img');
  img$$.classList.add('receta__img');
  img$$.src = post.img;
  const p$$ = document.createElement('p');
  p$$.textContent = post.body;   ///en principio todo pero tengo que hacer un for que me coja solo el principio

  const creadoTxt$$ = document.createElement('h3');
  creadoTxt$$.textContent = "Receta compartida por:";
  const creador$$ = document.createElement('h3');
  creador$$.textContent = buscadorUsuarioId(post.userId).name;

  main$$.appendChild(h1$$);
  main$$.appendChild(hr$$);
  main$$.appendChild(fecha$$);
  main$$.appendChild(img$$);
  main$$.appendChild(p$$);
  main$$.appendChild(creadoTxt$$);
  main$$.appendChild(creador$$);

  const Comentarios$$ = document.createElement('div');
  Comentarios$$.classList.add('main__receta__comens');
  const tituloComen$$ = document.createElement('h2');
  tituloComen$$.textContent = "Comentarios";

  if((commentsArray.length>0)||(ComSum > 0)){
    Comentarios$$.appendChild(tituloComen$$);
  }

  for (let i=0 ; i < commentsArray.length; i++){
    const comment$$ = componenteComentario(commentsArray[i]);
    Comentarios$$.appendChild(comment$$);
  }

  //Miramos si hay mas comentarios que pintar en el local storage
  
  if (ComSum > 0){
    for(let i=1; i<=ComSum; i++){
      
      const Comen = localStorage.getItem('Post' + PostId + '_Com' + i);
      
      const guion1 = Comen.search('_');
      const comentario = Comen.slice(0, guion1);
      
      const Comen2 = Comen.slice(guion1 + 1);
      const guion2 = Comen2.search('_');
      const userId = Comen2.slice(0, guion2);

      const date = Comen2.slice(guion2 + 1);

      const Comentario = {
        "userId": parseInt(userId),
        "postId": PostId,
        "id": null,
        "date": date,
        "text": comentario
      }
      const commentLocal$$ = componenteComentario(Comentario);
      Comentarios$$.appendChild(commentLocal$$);
    }
  }
 
  //Miramos si estamos logueados y si hay comentarios dejados
  if(localStorage.getItem('log') === 'true'){
    const comentar$$ = componenteDejarComentario();
    Comentarios$$.appendChild(comentar$$);
  }
  main$$.appendChild(Comentarios$$);
  blog$$.appendChild(main$$);
}

function ObtenerComentarios(main$$, post){
  fetch('http://localhost:3000/comments').then(res => res.json()).then(function(comments){
    for (const comment of comments) {
      if (post.id === comment.postId){
        commentsArray.push(comment);
      }
    }
    postReceta(main$$, post);
    });
}


function componenteComentario(comment){
  const comenDiv$$ = document.createElement('div');
  comenDiv$$.classList.add('main__receta__comens__comp');

  const comenTxtDiv$$ = document.createElement('div');
  comenDiv$$.classList.add('main__receta__comens__comp__txt');

  const comenFoto$$ = document.createElement('img');
  comenFoto$$.classList.add('main__receta__comens__comp__img');
  comenFoto$$.src = buscadorUsuarioId(comment.userId).img;

  const h3$$ = document.createElement('h3');
  h3$$.textContent = buscadorUsuarioId(comment.userId).name;

  const pDate$$ = document.createElement('p');
  pDate$$.textContent = comment.date;

  const p$$ = document.createElement('p');
  p$$.textContent = comment.text;

  comenTxtDiv$$.appendChild(h3$$);
  comenTxtDiv$$.appendChild(pDate$$);
  comenTxtDiv$$.appendChild(p$$);

  comenDiv$$.appendChild(comenFoto$$);
  comenDiv$$.appendChild(comenTxtDiv$$);
  return comenDiv$$;
}

function componenteDejarComentario(){
  const DejacomenDiv$$ = document.createElement('div');
  DejacomenDiv$$.classList.add('main__receta__comens__dejar');

  const h2$$ = document.createElement('div');
  h2$$.textContent = "Comentar";
  h2$$.classList.add('main__receta__comens__dejar__h2');

  const form$$ = document.createElement('form');
  form$$.classList.add('main__receta__comens__dejar__form')

  const inputComen$$ = document.createElement('input');
  inputComen$$.setAttribute('type', "text");
  inputComen$$.setAttribute('maxlength', "1000");
  //inputComen$$.setAttribute('wrap', "hard");
  inputComen$$.classList.add('main__receta__comens__dejar__form__input');

  const btn$$ = document.createElement('button');
  btn$$.classList.add('main__receta__comens__dejar__form__btn');
  btn$$.textContent = "Listo";

  btn$$.addEventListener('click', Comentar);

  form$$.appendChild(inputComen$$);
  form$$.appendChild(btn$$);

  DejacomenDiv$$.appendChild(h2$$);
  DejacomenDiv$$.appendChild(form$$);

  return DejacomenDiv$$;
}

///**************************************** USUARIOS ************************************************************/

function ObtenerUsuarios(){
  fetch('http://localhost:3000/users').then(res => res.json()).then(function(users){
    for (const user of users) {
        usersArray.push(user);
    }
    });
}

function buscadorUsuarioId(id){
  for (const user of usersArray) {
    if(id === user.id){
      return user;
    }
  }
}

function buscadorUsuarioEmailPass(email, pass){
  for (const user of usersArray) {
    if((email === user.email) && (pass === user.password)){
      return user;
    }
  }
}

/**************************************** LOGIN ****************************************************************/

function FormatoLogin(nav$$){
  const div$$ = document.createElement('div');
  div$$.classList.add('componente__login')

  const logo$$ = document.createElement('img');
  logo$$.src = '/src/assets/img/logo_pq.png';

  const h1$$ = document.createElement('h1');
  h1$$.textContent = "Entra en tu área";

  const form$$ = document.createElement('form');
  form$$.classList.add('componente__login__form')

  const inputEmail$$ = document.createElement('input');
  inputEmail$$.setAttribute('type', "email");
  inputEmail$$.setAttribute('placeholder', "Correo electrónico");
  inputEmail$$.classList.add('componente__login__form__email');

  
  const inputPassword$$ = document.createElement('input');
  inputPassword$$.setAttribute('type', "password");
  inputPassword$$.setAttribute('placeholder', "Contraseña");
  inputPassword$$.classList.add('componente__login__form__pass');

  /*const inputCheckbox$$ = document.createElement('input');
  inputCheckbox$$.setAttribute('type', "checkbox");
  inputCheckbox$$.setAttribute('id', "checkbox_recuerdame");
  
  const label$$ = document.createElement('label');
  label$$.textContent = "Recuérdame";
  label$$.setAttribute('for', "checkbox_recuerdame");*/

  const btn$$ = document.createElement('button');
  btn$$.classList.add('componente__login__form__btn');
  btn$$.textContent = "Entrar";

  form$$.appendChild(inputEmail$$);
  form$$.appendChild(inputPassword$$);
  form$$.appendChild(btn$$);

  div$$.appendChild(logo$$);
  div$$.appendChild(h1$$);
  div$$.appendChild(form$$);

  //div$$.appendChild(inputEmail$$);
  //div$$.appendChild(inputPassword$$);
  /*div$$.appendChild(inputCheckbox$$);
  div$$.appendChild(label$$);*/
  //div$$.appendChild(btn$$);

  nav$$.appendChild(div$$);
}


function login (nav$$){
  FormatoLogin(nav$$);
  const menu$$ = document.querySelector('.hamburger');
  const btn$$ = document.querySelector('.componente__login__form__btn'); 

  menu$$.addEventListener('click', function(e) {
    reset();
  });

  //event listener al boton:
  btn$$.addEventListener('click', CheckLogin);

}

const CheckLogin = (event) => {
  let Email = document.querySelector('.componente__login__form__email').value;
  let Password = document.querySelector('.componente__login__form__pass').value;

  const Password$$ = document.querySelector('.componente__login__form__pass');
  
  let UserNow = buscadorUsuarioEmailPass(Email, Password);
  if (UserNow !== null){
    const error$$ = document.querySelector('.componente__login__form__error');
    if( error$$ != null){
      error$$.remove();
    }
    //Guardo el log como true
    localStorage.setItem('log', 'true');
    //Busco y guardo el usuario que está logueado
    localStorage.setItem('UserId', UserNow.id);
    document.querySelector('.hamburger').click();
    
  }else{
    if(document.querySelector('.componente__login__form__error') === null){
      const error$$ = document.createElement('p');
      error$$.classList.add('componente__login__form__error');
      error$$.textContent = "Correo o contraseña erróneos";
      error$$.style.color= 'red';
      insertAfter(error$$, Password$$);
    }
  }
}

const Comentar = (event) => {
  let comentario = document.querySelector('.main__receta__comens__dejar__form__input').value;
  let ComSum;
  if(!parseInt(localStorage.getItem('PostSum' + PostId))){
    ComSum = 1;
  }else{
    ComSum = parseInt(localStorage.getItem('PostSum' + PostId)) + 1;
  }
    
  localStorage.setItem('PostSum' + PostId, ComSum);

  const UserNow_Id = parseInt(localStorage.getItem('UserId'));
  localStorage.setItem('Post' + PostId + '_Com' + ComSum, comentario + '_' + UserNow_Id + '_' + formatDate());

  const mainReceta$$ = document.querySelector('.main__receta'); 
  postReceta(mainReceta$$, postComp);
}


function limpiarLocalStorage(){
  localStorage.clear(); 
}

function formatDate(){
  let date = new Date()

  let day = date.getDate()
  let month = date.getMonth() + 1
  let year = date.getFullYear()

  if(day < 10){
    day = '0' + day;
  }if (month <10){
    month = '0' + month;
  }

  return day + '-' + month + '-' + year;
}



